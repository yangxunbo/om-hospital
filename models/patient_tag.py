from odoo import api, fields, models, _, tools


class PatientTag(models.Model):
    _name = 'patient.tag'
    _rec_name = 'name'
    _description = 'Patient Tag'

    name = fields.Char(string="Tag Name", required=False, )
    active = fields.Boolean(string="Active", default=True)
    color = fields.Integer(string="Color", required=False, copy=False, )
    color_tow = fields.Char(string="Color Tow")
    sequence = fields.Integer(string="Sequence", required=False, )

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        if default is None:
            default = {}
        print('1.->', default)
        print('2.->', default.get('name'))
        if not default.get('name'):
            print('3.->', default)
            print('4.->', default.get('name'))
            default['name'] = _("%s (copy)", self.name)
            print('5.->', default)
            print('6.->', default.get('name'))

        return super(PatientTag, self).copy(default)

    _sql_constraints = [
        ('unique_tag_name', 'unique(name,active)', 'Tag Name must be unique!'),
        ('check_sequence', 'check(sequence > 0)', 'Sequence max zero!')
    ]
