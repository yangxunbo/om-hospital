# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools
from odoo.exceptions import ValidationError


class HospitalAppointment(models.Model):
    _name = 'hospital.appointment'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Hospital Appointment'
    _rec_name = 'name'

    # @api.model
    # def default_get(self, fields_list):
    #     rst = super(HospitalAppointment, self).default_get(fields_list)
    #     if not rst.get('name'):
    #         rst['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment')
    #         return rst

    name = fields.Char(string="Sequences", required=False, )
    patient_id = fields.Many2one(comodel_name="hospital.patient", string="Patient", ondelete='restrict')
    # patient_id = fields.Many2one(comodel_name="hospital.patient", string="Patient", ondelete='cascade')
    appointment_time = fields.Datetime(string="Appointment Time", default=fields.Datetime.now)
    booking_date = fields.Date(string="Booking Date", default=fields.Date.context_today)
    gender = fields.Selection(related='patient_id.gender', readonly=True)
    age = fields.Integer(string="Age", related='patient_id.age', )
    ref = fields.Char(string="Reference", help="test Help")
    prescription = fields.Html(string="Prescription")
    priority = fields.Selection(string="priority", selection=[
        ('0', 'Normal'),
        ('1', 'Low'),
        ('2', 'High'),
        ('3', 'Very High'),
    ])

    state = fields.Selection(string="Status", selection=[
        ('draft', 'Draft'),
        ('start', 'Start'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], default='draft', required=True, tracking=True)
    doctor_id = fields.Many2one(comodel_name="res.users", string="Doctor", required=False, )
    pharmacy_line_ids = fields.One2many(comodel_name="appointment.pharmacy.lines", inverse_name="appointment_id",
                                        string="Pharmacy Line", required=False, )
    hide_sales_price = fields.Boolean(string="Hide Sales Price", default=True, )
    reason = fields.Text(string="reason", required=False, tracking=True)

    @api.onchange('patient_id')
    def onchange_patient_id(self):
        self.ref = self.patient_id.ref

    def action_start(self):
        for rec in self:
            if rec.state == 'draft':
                rec.state = 'start'

    # def action_cancel(self):
    #     for rec in self:
    #         rec.state = 'cancel'

    def action_cancel(self):
        action = self.env.ref('om_hospital.action_cancel_appointment').read()[0]
        return action

    def action_draft(self):
        for rec in self:
            rec.state = 'draft'

    def action_done(self):
        for rec in self:
            rec.state = 'done'

        return {
            'effect': {
                'fadeout': 'slow',
                'message': 'Clik Successfull',
                'type': 'rainbow_man'
            }
        }

    @api.model
    def create(self, vals_list):
        if not self.name:
            vals_list['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment')
            return super(HospitalAppointment, self).create(vals_list)

    # def write(self, vals):
    #     if not self.name:
    #         vals['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment')
    #         return super(HospitalAppointment, self).write(vals)

    def unlink(self):
        if self.state != 'draft':
            raise ValidationError(_("you cannot delete %s appointment!", self.state))
        return super(HospitalAppointment, self).unlink()


class AppointmentPharmacyLines(models.Model):
    _name = 'appointment.pharmacy.lines'
    _rec_name = 'product_id'
    _description = 'Appointment Pharmacy Lines'

    product_id = fields.Many2one(comodel_name="product.product", string="Product", required=True, )
    pirce_unit = fields.Float(string="Price", related='product_id.list_price')
    qyt = fields.Integer(string="Quantity", required=False, default=1)
    appointment_id = fields.Many2one(comodel_name="hospital.appointment", string="", required=False, )
