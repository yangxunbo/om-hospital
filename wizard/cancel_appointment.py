import datetime
from odoo import fields, models, api,_
from odoo.exceptions import ValidationError


class CancelAppointmentWizard(models.TransientModel):
    _name = 'cancel.appointment.wizard'
    _description = 'Cancel Appointment Wizard'

    @api.model
    def default_get(self, fields_list):
        # print('1.->',fields_list)
        print("1.->", self.env.context)
        res = super(CancelAppointmentWizard, self).default_get(fields_list)
        res['data_cancel'] = datetime.date.today()
        if self.env.context.get('active_id'):
            res['appointment_id'] = self.env.context.get('active_id')
        return res

    appointment_id = fields.Many2one(comodel_name="hospital.appointment", string="Appointment", required=False,)
    reason = fields.Text(string="reason", required=False, )
    data_cancel = fields.Date(string="Cancellation", required=False, )

    def confirm(self):
        if self.appointment_id.booking_date == fields.Date.today():
            raise ValidationError(_("Sorry,you are not today booking!"))
        self.appointment_id.reason = self.reason
        self.appointment_id.state = 'cancel'
